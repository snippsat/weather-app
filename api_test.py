import requests

# Call example
#http://api.openweathermap.org/data/2.5/weather?q=London&units=metric&APPID=xxxxxxxxxxxxx

api_key = 'xxxxxxxxxxxxxxxx'
city = 'london'
celsius = 'metric'
url = f'http://api.openweathermap.org/data/2.5/weather?q={city}&units={celsius}&APPID={api_key}'
response = requests.get(url).json()

# City
print(response.get('name'))

# Temp
print(response['main'].get('temp'))

# Description
print(response['weather'][0].get('description'))

# Wind
print(response['wind'].get('speed'))

''' Output-->
London
22.89
clear sky
3.6
'''